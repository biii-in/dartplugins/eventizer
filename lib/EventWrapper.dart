import 'package:eventizer/Eventizer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:toast/toast.dart';

const EventID _toastDisplay = const EventID(dstr: 'GToast');

const EventID _dialogDisplay = const EventID(dstr: 'GDialog');

class EventWrapper extends StatefulWidget {
  final Widget child;

  EventWrapper({@required this.child});

  @override
  _EventWrapperState createState() => _EventWrapperState();
}

class _EventWrapperState extends State<EventWrapper> with EventConsumer {
  @override
  void initState() {
    super.initState();
    addEventHandler([_toastDisplay, _dialogDisplay]);
  }

  @override
  Widget build(BuildContext context) => widget.child;

  @override
  void onEvent(Event event) {
    switch (event.id) {
      case _toastDisplay:
        Toast.show(
          event.get<String>(),
          context,
          backgroundColor: event.get<MaterialColor>() ?? event.get<Color>(),
          textColor: Colors.white,
        );
        break;
      case _dialogDisplay:
        showDialog(
          context: context,
          builder: (context) => event.get('b')(context),
          barrierDismissible: event.get<bool>('m'),
        );
        break;
    }
  }
}

showToastEvent(msg, {bgColor = Colors.grey}) =>
    _toastDisplay.fireWith([msg, bgColor]);

showDialogEvent({WidgetBuilder builder, modal}) =>
    _dialogDisplay.fireWith({'b': builder, 'm': !(modal ?? false)});
