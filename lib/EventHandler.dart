import 'package:biii_in_utils/biii_in_utils.dart';
import 'package:eventizer/Eventizer.dart';

/// final but private [Map] of [List] of [EventConsumer]
/// this structure holds all active listener for a perticular [EventID]
/// this structure if used whenever a event with EventID is triggered
/// TODO make it thread safe
final Map<EventID, List<EventConsumer>> _consumerList = {};

/// abstarct event consumer class because each event is unique in its own way,
/// its handling is also unique thus this class is abstract
/// user must extend, mixin with this class to use the power of eventizer consumer interface
/// optionally user can also set debug tag
abstract class EventConsumer {
  /// adds this EventConsumer entry to the global list for the provided list of EventID [list]
  void addEventHandler(List<EventID> list) {
    Log.i(this, 'Registered for $list');
    list.forEach((id) {
      _consumerList[id] ??= [];
      if (!_consumerList[id].contains(this)) {
        _consumerList[id].add(this);
      }
    });
  }

  /// removes this EventConsumer entry from the global list for the provided list of EventID [list]
  void removeEventHandler(List<EventID> list) {
    Log.i(this, 'Removed for $list');
    list.forEach((id) {
      _consumerList[id]?.remove(this);
    });
  }

  /// this method must be implemented by the extender of [EventConsumer]
  ///
  /// this api is called by [EventProducer] whenever an event is triggered
  void onEvent(Event event);
}

/// if a class is expected to generate event (mainly ui or async fucntion).
/// then they must extend, mixin with this class to trigger events
class EventProducer {
  //
  // String _debugTag;

  // set debug tag to [dt]
  // set debugTag(dt) => _debugTag = dt;

  /// trigger this [event] and notifies all registered consumer with optional [args]
  void triggerEvent(Event event) {
    Future.delayed(Duration.zero, () {
      Log.i(this, 'Event Fired ${event.id} with ${event.data}');
      _consumerList[event.id]?.forEach((handler) => handler.onEvent(event));
    });
  }
}
