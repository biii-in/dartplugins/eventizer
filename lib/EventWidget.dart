import 'package:biii_in_utils/biii_in_utils.dart';
import 'package:eventizer/Eventizer.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';

// const EventTester _trueOnly =
bool _trueOnlyTester(list) => true;

/// typedef for a function prototype used for eventTester
typedef EventTester = bool Function(Event);

/// typedef to facillitates a task trigger when widget is ready to take event
typedef WidgetNotifier = Function();

/// global constant EventTester that passes all event
const EventTester eventTesterPassAll = _trueOnlyTester;

/// typedef builder function prototype used by EventWidget
typedef EventItemBuilder = Widget Function(
    BuildContext context, Widget child, Event event);

/// dynamic widget that changed whenever a register event is trigger and passed by event tester
class EventWidget extends StatefulWidget with EventProducer {
  /// final child that was passed to builder function
  final Widget child;

  /// final childPlaceHolder that is used if initallly there is no event to process and it was also not null
  final Widget childPlaceHolder;

  /// EventID list to be consumed with passed EventTester
  final Map<EventID, EventTester> consumers;

  /// builder function that is called whenever event passes a test after getting triggered
  final EventItemBuilder builder;

  /// ready for event notifier funtion
  final WidgetNotifier onReady;

  /// first event to be tested while building this widget for first time
  final Event buildEvent;

  /// debug tag to help with debugging
  final String debugTag;

  EventWidget({
    this.buildEvent,
    @required this.consumers,
    @required this.builder,
    this.onReady,
    this.child,
    this.childPlaceHolder =
        kDebugMode ? const Text('Empty Event') : const SizedBox.shrink(),
    this.debugTag = 'EventWidget',
    Key key,
  })  : assert(consumers != null),
        assert(builder != null),
        super(key: key);

  @override
  State<StatefulWidget> createState() => _EventWidgetState();
}

class _EventWidgetState extends State<EventWidget> with EventConsumer {
  Event _event;

  @override
  void initState() {
    super.initState();
    // register its event Handlers
    addEventHandler(widget.consumers.keys.toList());
    //
    if (widget.buildEvent != null) {
      onEvent(widget.buildEvent);
    }
    //
    if (widget.onReady != null) {
      widget.onReady();
    }
  }

  @override
  void dispose() {
    //
    removeEventHandler(widget.consumers.keys.toList());
    //
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (_event == null) {
      return widget.childPlaceHolder != null
          ? widget.childPlaceHolder
          : widget.child != null
              ? widget.child
              : widget.builder(context, widget.child, _event);
    } else {
      return widget.builder(context, widget.child, _event);
    }
  }

  @override
  void onEvent(Event event) {
    assert(event.id != null, '${event.id} cannot be null');
    assert(widget.consumers[event.id] != null,
        'EventTester for ${event.id} not Found');
    if (widget.consumers[event.id](event) ?? false) {
      Log.i(widget.debugTag, 'rebuilding');
      setState(() {
        _event = event;
      });
    }
  }
}
