import 'package:eventizer/Eventizer.dart';

/// just a placeholder class to ease internal type checks
/// user should extend it as per their need
///
class EventID with EventProducer {
  /// debug string only used if eventizer's log was enabled
  final String dstr;

  /// create event with debug string [dstr] passed
  const EventID({this.dstr = 'GEvent'});

  /// simply return debug string
  @override
  String toString() => dstr;

  /// facilator api to return an [Event] Object encapsulating this [EventID]
  /// with passed [data]
  Event toEvent([data]) {
    if (data is List<Object>) {
      return EventMulti(this, data);
    } else if (data is Map<Object, Object>) {
      return EventMap(this, data);
    } else {
      return Event(this, data);
    }
  }

  /// facilator api to create and fire an [Event] Object encapsulating this [EventID]
  /// with passed [data]
  void fireWith([data]) => triggerEvent(this.toEvent(data));
}
