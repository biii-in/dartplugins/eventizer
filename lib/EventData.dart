import 'package:eventizer/Eventizer.dart';

/// [Event] Class defines a skeletone for basic event
///
/// this class holds a single data as its argument that is optional also
/// this should be used for simple notification events that either doen't
/// require any data or single data for its operation
class Event {
  /// [id] of this event
  ///
  /// This help identify consumer to cosumer its data accordingly
  final EventID id;

  /// [data] data supplied with this event to facilitate event's consumption
  /// format of event should be predefined by the implementer
  final dynamic data;

  /// Construct Event Data object with passed [id] and optional [data] with a Typed type
  ///
  /// [id] events identification object
  /// [data] events data to be packed with this event. this is optional parameter
  /// becuase this event can be notification only
  /// or this event uses global variables for its consumption
  Event(this.id, [data]) : this.data = data;

  /// [id] optional parameter can be anything
  ///
  /// this is ignored in simple event structure,
  /// included here to unify get api for complex data structure
  R get<R>([id = 0]) {
    assert(R == dynamic || R == Object || (data?.runtimeType ?? R) == R,
        'type mismatch expected $R found ${data?.runtimeType}');
    return data;
  }
}

/// [Event] Class defines a skeletone for comples event
///
/// this class holds a single data as its argument that is optional also
/// this should be used for simple notification events that either doen't
/// require any data or single data for its operation
class EventMulti extends Event {
  EventMulti(id, List<Object> args) : super(id, {}) {
    args.forEach(
      (e) {
        data[e.runtimeType] = (data[e.runtimeType] ?? []);
        data[e.runtimeType].add(e);
      },
    );
  }

  /// Get Data of provide Type [R] and at an index [id]
  ///
  /// [id] optional index value as int
  /// [R] type of argument expected
  @override
  R get<R>([id = 0]) {
    assert((data[R]?.length ?? 0) > id,
        'index out of boud ${data[R]?.length} $id');
    assert(R == dynamic || R == Object || (data[R][id]?.runtimeType ?? R) == R,
        'type mismatch expected $R found ${data[R][id]?.runtimeType}');
    return data[R][id];
  }
}

/// [EventMap] Class defines a skeletone for comples event
///
/// this class holds a single data as its argument that is optional also
/// this should be used for simple notification events that either doen't
/// require any data or single data for its operation
class EventMap extends Event {
  EventMap(id, Map data) : super(id, data);

  /// Get Data of provide Type [R] and at an index [id]
  ///
  /// [id] optional index value as int
  /// [R] type of argument expected
  @override
  R get<R>([id = 0]) {
    assert(R == dynamic || R == Object || (data[id]?.runtimeType ?? R) == R,
        'type mismatch expected $R found ${data[id]?.runtimeType}');
    return data[id] as R;
  }
}
