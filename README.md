# Eventizer

Eventizer will help you in decoupling of your ui code and logic code. with eventizer you have single link between all ui element and logic element. it will also help in modular architecture.

## Event Widget Usage

1. Wrap a dynamic widget in `EventWidget` with the help of `builder`
   e.g.
   ```
   EventWidget(
    builder: (context, widget, event) => 
          Text('${event.get<int>()}'),
   ),
   ```

2. Then specify events this widget will capture and test using `consumers`. `consumers` is a Map structure with `EventID` as key and a tester Function as value. whenever event with `EventID` is fired this widget will catch it and execute tester function. if tester return true Widget will rebuild.
   e.g.
   ```
    consumers: {someEventID: testFunction(event)},
   ```

3. Then Whenever and Wherever you need to rebuild above Widget. You just need to fire the event with or without data
   e.g.
   ```
   someEventID.fireWith(data);
   ```

## Aditionals

1. Above `EventWidget` will draw `SizedBox.shrink()` in release mode and `Text('Empty Widget')` in debug mode till a event id fired. but what if we need something prior to loading. There are multiple ways to handle this
   * `childPlaceHolder` property will be returned if no event has been captured yet and it.
   * `child` will be returned if `childPlaceHolder` is null and no event has been captured yet
   * you can even specify `buildEvent`. then EventWidget will be initialized as if it has captured passed `buildEvent`.

2. Flutter rebuild is not instantaneous but scheduled. So it might be possible event just added will miss a fast event. in this case you can start your task just after `EventWidget` is built using `onReady` Callback property.


## Event

There are multiple ways data is encapsulate in an event object

1. `Event` is fired without any data using `event.fireWith()`. most suitable for notifications.
2. `Event` if fire with single data object using `event.fireWith(data)`. In this case you can extract data simply by using `event.get()`.
3. `Event` is fired with List of data, preferable of unique type, using `event.fireWith([data1,data2,...])`. In this case you can extract data from event using `event.get<type>(id)`.
4. `Event` is fired with List of data, preferable of unique type, using `event.fireWith({key1:value1,key2,value2})`. In this case you can extract data from event using `event.get<type>(key)`.
   
*note: type parameter is optional in above APIs*