# Gitlab Examples

### [1. Default Flutter Template App Example](https://gitlab.com/biii-in/dartplugins/eventizer/-/blob/master/example/FlutterAppExample.md)

### [2. How to Rebuild Asynchronously](https://gitlab.com/biii-in/dartplugins/eventizer/-/blob/master/example/AsynchronousRebuild.md)

### [3. How to rebuild With Selection](https://gitlab.com/biii-in/dartplugins/eventizer/-/blob/master/example/AsynchronousRebuildSelection.md)