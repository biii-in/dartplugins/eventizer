## EventWidget Rebuild Example


### 1. Create An Event
```
const EventID incrimentEvent = const EventID(dstr: 'incrimentEvent');
```
Creating a event is simple, just instantiate `EventID` class with a unique string, remember your event should be const, and to satisfy this requirement we have used a debug String (`dstr`) that should be unique. This thing helps dart in creating compile time constants.

### 2. Create An EventWidget
```
EventWidget(
    consumers: {incrimentEvent: eventTesterPassAll},
    builder: (context, widget, event) => Text('${event.get<int>()}'),
),
```
This is Text Widget enclosed by a EventWidget, Event widget require two parameter for assert free operations

- `consumers`: this is a map structure that holds events as key and tester function as value
- `builder` function: This will get called if a event registered in consumer has passed through a tester function

in the example code provided we have registerd an event Widget for `incrementEvent` having `eventTesterPassAll` as tester function, this tester function is a placeholder to return true, that means if `incrementEvent` is fired then `builder` function will be called for sure.

### 2. Rebuild it Asynchronously

```
incrementEvent.fireWith();
```
To rebuild this widget from anywhere, you just need to call `incrementEvent.fireWith()` function. that's it, `no context` or `reference` is required, you just need to create a global constant `incrimentEvent`.
