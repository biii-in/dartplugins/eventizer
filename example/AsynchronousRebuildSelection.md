## EventWidget Rebuild With Selection Example

### 1. Create An Event
```
const EventID timerEvent = const EventID(dstr: 'timerEvent');
const EventID timerResetEvent = const EventID(dstr: 'timerResetEvent');
```
Creating a event is simple, just instantiate `EventID` class with a unique string, remember your event should be const, and to satisfy this requirement we have used a debug String (`dstr`) that should be unique. This thing helps dart in creating compile time constants.

## data is supplied as List and used by Type

### 2. Create Row with 3 EventWidget each for hour/min/sec
```
Row(
    childerns: [
        EventWidget(
            consumers: {
                timerResetEvent: eventTesterPassAll,
                timerEvent: (event)=>event.get<String>() == 'H'
            },
            builder: (context, widget, event) => Text('${event.get<int>()}'),
        ),
        EventWidget(
            consumers: {
                timerResetEvent: eventTesterPassAll,
                timerEvent: (event)=>event.get<String>() == 'M'
            },
            builder: (context, widget, event) => Text('${event.get<int>()}'),
        ),
        EventWidget(
            consumers: {
                timerResetEvent: eventTesterPassAll,
                timerEvent: (event)=>event.get<String>() == 'S'
            },
            builder: (context, widget, event) => Text('${event.get<int>()}'),
        ),
    ]
)

```
This is Text Widget enclosed by a EventWidget, Event widget require two parameter for assert free operations

- `consumers`: this is a map structure that holds events as key and tester function as value
- `builder` function: This will get called if a event registered in consumer has passed through a tester function

in the example code provided we have registerd 3 event Widget for `timerEvent` but having different tester function, for Hour Widget we test events String Data is `'H'` and thus it will return true only if `'H'` is passed with event thus rebuilding hour widget only for hour event. similary Minute and Seconds widget will rebuild only for their respective events. timerResetEvent is also added to all widget with `eventTesterPassAll`. that will rebuild all widget.

### 2. Rebuild it Asynchronously

```
// update hour
timerEvent.fireWith(['H',23]);
// update Minute
timerEvent.fireWith(['M',59]);
// update Seconds
timerEvent.fireWith(['S',32]);
// reset all
timerResetEvent.fireWith([0]);
```

use above function to update timer widgets from anywhere of your code, remember because each widget depends on int type data supplied by event we have to pass 0 with timerResetEvent also. or you can perform additional checks in builder function
